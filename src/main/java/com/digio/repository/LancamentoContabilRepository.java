package com.digio.repository;

import org.springframework.data.repository.CrudRepository;

import com.digio.entity.ContaContabil;
import com.digio.entity.LancamentoContabil;

public interface LancamentoContabilRepository extends CrudRepository<LancamentoContabil, String> {
	
	public Iterable<LancamentoContabil> findAllByContaContabil(ContaContabil conta);
	
}

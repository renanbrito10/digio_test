package com.digio.configuration;

import javax.persistence.EntityNotFoundException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { EntityNotFoundException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest req) {
        String body = "Registro nao encontrado";
        return handleExceptionInternal(ex, body, new HttpHeaders(), HttpStatus.CONFLICT, req);
    }
}

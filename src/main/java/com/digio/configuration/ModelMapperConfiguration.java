package com.digio.configuration;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConfiguration {

	@Bean
	public ModelMapper modelMapper() {
		ModelMapper modelMapper = new ModelMapper();

	    Converter<Integer, LocalDate> toIntegerDate = (context) -> {
            DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyyMMdd");
            return LocalDate.parse(context.getSource().toString(), format);
	    };

	    Converter<LocalDate, Integer> toDateInteger = (context) -> {
	        return Integer.valueOf(context.getSource().format(DateTimeFormatter.ofPattern("yyyyMMdd")));
	    };

	    modelMapper.addConverter(toIntegerDate);
		modelMapper.addConverter(toDateInteger);
		return modelMapper;
	}
}

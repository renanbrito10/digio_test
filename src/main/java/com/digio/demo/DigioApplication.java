package com.digio.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"com.digio"})
public class DigioApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigioApplication.class, args);
	}

}

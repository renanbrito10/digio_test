package com.digio.entity;

import java.io.Serializable;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="DG_LANCAMENTO_CONTABIL")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LancamentoContabil implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private String id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private ContaContabil contaContabil;
	
	@Column
	private LocalDate data;
	
	@Column
	private Double valor;



}

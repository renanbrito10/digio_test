package com.digio.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="DG_CONTA_CONTABIL")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContaContabil implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	private Integer id;

}

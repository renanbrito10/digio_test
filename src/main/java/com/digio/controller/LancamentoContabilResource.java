package com.digio.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digio.dto.LancamentoContabilDTO;
import com.digio.dto.LancamentoContabilIdDTO;
import com.digio.dto.LancamentoContabilStatsDTO;
import com.digio.service.LancamentoContabilService;

@RestController
@RequestMapping("/lancamentos-contabeis")
public class LancamentoContabilResource {

	@Autowired
	LancamentoContabilService lancamentoContabilService;
	
	@PostMapping
	public ResponseEntity<LancamentoContabilIdDTO> create(@Valid @RequestBody LancamentoContabilDTO conta) {
		return new ResponseEntity<LancamentoContabilIdDTO>(lancamentoContabilService.create(conta), HttpStatus.CREATED);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<LancamentoContabilDTO> findById(@PathVariable(name="id", required = true) String id) {
		return ResponseEntity.ok(lancamentoContabilService.findById(id));
	}
	
	@GetMapping()
	public ResponseEntity<List<LancamentoContabilDTO>> findAllByConta(@RequestParam(name = "contaContabil", required = true) Integer idConta) {
		return ResponseEntity.ok(lancamentoContabilService.findAllByConta(idConta));
	}
	
	@GetMapping("/stats")
	public ResponseEntity<LancamentoContabilStatsDTO> findStatsByConta(@RequestParam(name = "contaContabil", required = false) Integer idConta) {
		return ResponseEntity.ok(lancamentoContabilService.findStatsByConta(idConta));
	}
	
}

package com.digio.service;

import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.digio.dto.LancamentoContabilDTO;
import com.digio.dto.LancamentoContabilIdDTO;
import com.digio.dto.LancamentoContabilStatsDTO;
import com.digio.entity.ContaContabil;
import com.digio.entity.LancamentoContabil;
import com.digio.repository.LancamentoContabilRepository;

@Service
public class LancamentoContabilServiceImpl implements LancamentoContabilService {

	@Autowired
	LancamentoContabilRepository lancamentoContabilRepository;
	
	@Autowired
	ModelMapper modelMapper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
	public LancamentoContabilIdDTO create(LancamentoContabilDTO lancamentoDTO) {
		LancamentoContabil lancamento = modelMapper.map(lancamentoDTO, LancamentoContabil.class);
		return modelMapper.map(lancamentoContabilRepository.save(lancamento), LancamentoContabilIdDTO.class);
	}
	
	@Override
	public LancamentoContabilDTO findById(String id) {
		Optional<LancamentoContabil> lancamento = lancamentoContabilRepository.findById(id);
		LancamentoContabilDTO lancamentoDTO = null; 
		if (lancamento.isPresent()) {
			 lancamentoDTO = modelMapper.map(lancamento, LancamentoContabilDTO.class);
		}
		lancamento.orElseThrow(() -> new ResourceNotFoundException("Nao ha lancamentos com esse Id."));
		return lancamentoDTO;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LancamentoContabilDTO> findAllByConta(Integer idConta) {
		Iterable<LancamentoContabil> lancamentosItarable = lancamentoContabilRepository.findAllByContaContabil(new ContaContabil(idConta));
		List<LancamentoContabil> lancamentos = StreamSupport.stream(lancamentosItarable.spliterator(), false)
														.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(lancamentos)) {
			throw new ResourceNotFoundException("Nao ha lancamentos para essa conta.");
		}
		return modelMapper.map(lancamentos, ArrayList.class);
	}
	
	@Override
	public LancamentoContabilStatsDTO findStatsByConta(Integer idConta) {
		Iterable<LancamentoContabil> stats = null;
		if (idConta != null && idConta > 0) {
			stats = lancamentoContabilRepository.findAllByContaContabil(new ContaContabil(idConta));
		} else {
			stats = lancamentoContabilRepository.findAll();
		}
		return createStats(stats);
	}
	
	private LancamentoContabilStatsDTO createStats(Iterable<LancamentoContabil> stats) {
		DoubleSummaryStatistics summaryStatistics = StreamSupport.stream(stats.spliterator(), false)
				.mapToDouble(LancamentoContabil::getValor)
				.summaryStatistics();

		if (summaryStatistics.getCount() == 0) {
			throw new ResourceNotFoundException("Nao ha lancamentos para gerar estatistica.");
		}
		
		LancamentoContabilStatsDTO lancamentoContabilStats = new LancamentoContabilStatsDTO();
		lancamentoContabilStats.setMaximo(summaryStatistics.getMax());
		lancamentoContabilStats.setMinimo(summaryStatistics.getMin());
		lancamentoContabilStats.setSoma(summaryStatistics.getSum());
		lancamentoContabilStats.setMedia(summaryStatistics.getAverage());
		lancamentoContabilStats.setQuantidade(summaryStatistics.getCount());
		return lancamentoContabilStats;
	}

}

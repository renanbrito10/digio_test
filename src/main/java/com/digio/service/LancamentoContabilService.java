package com.digio.service;

import java.util.List;

import com.digio.dto.LancamentoContabilDTO;
import com.digio.dto.LancamentoContabilIdDTO;
import com.digio.dto.LancamentoContabilStatsDTO;

public interface LancamentoContabilService {

	LancamentoContabilIdDTO create(LancamentoContabilDTO conta);

	LancamentoContabilDTO findById(String id);

	List<LancamentoContabilDTO> findAllByConta(Integer idConta);
	
	LancamentoContabilStatsDTO findStatsByConta(Integer idConta);
}

package com.digio.dto;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LancamentoContabilDTO {
	
	@JsonProperty(value = "contaContabil")
	@NotNull
	private Integer idContaContabil;
	
	@NotNull
	private Integer data;
	
	@NotNull
	private Double valor;

}

package com.digio.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LancamentoContabilStatsDTO implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Double soma;
	private Double minimo;
	private Double maximo;
	private Double media;
	private Long quantidade;

}

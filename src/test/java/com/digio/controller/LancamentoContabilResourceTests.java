package com.digio.controller;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.digio.demo.DigioApplication;
import com.digio.dto.LancamentoContabilDTO;
import com.digio.dto.LancamentoContabilIdDTO;
import com.digio.dto.LancamentoContabilStatsDTO;
import com.digio.service.LancamentoContabilService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {DigioApplication.class, LancamentoContabilResource.class})
public class LancamentoContabilResourceTests {

	private static final String LANCAMENTOS_CONTABEIS_STATS_PATH = "/lancamentos-contabeis/stats/";

	private static final String LANCAMENTOS_CONTABEIS_PATH = "/lancamentos-contabeis";

	@Autowired
	private WebApplicationContext context;
	
	@MockBean
	LancamentoContabilService service;
	
	private MockMvc mvc;
	
	@Before
	public void setup () {
		this.mvc = MockMvcBuilders.webAppContextSetup(this.context).build();
	}
	
	
	@Test
	public void testCreateSucesso() throws Exception {
		LancamentoContabilDTO requestDto = new LancamentoContabilDTO(1111001, 20170903, 140.90);
		LancamentoContabilIdDTO responseDto = new LancamentoContabilIdDTO("1");
		
		when(service.create(requestDto)).thenReturn(responseDto);
		mvc.perform(post(LANCAMENTOS_CONTABEIS_PATH)
					.content(new ObjectMapper().writeValueAsString(requestDto))
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("id", equalTo("1")));
	}
	
	@Test
	public void testCreateContaInexistente() throws Exception {
		LancamentoContabilDTO dto = new LancamentoContabilDTO(9999999, 20170903, 140.90);
		when(service.create(dto)).thenThrow(new EntityNotFoundException());
		mvc.perform(post(LANCAMENTOS_CONTABEIS_PATH)
					.content(new ObjectMapper().writeValueAsString(dto))
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isConflict());
	}
	
	@Test
	public void testCreateSemConta() throws Exception {
		LancamentoContabilDTO dto = new LancamentoContabilDTO(null, 20170903, 140.90);
		mvc.perform(post(LANCAMENTOS_CONTABEIS_PATH)
					.content(new ObjectMapper().writeValueAsString(dto))
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testCreateSemData() throws Exception {
		LancamentoContabilDTO dto = new LancamentoContabilDTO(1111001, null, 140.90);
		
		mvc.perform(post(LANCAMENTOS_CONTABEIS_PATH)
					.content(new ObjectMapper().writeValueAsString(dto))
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testCreateSemValor() throws Exception {
		LancamentoContabilDTO dto = new LancamentoContabilDTO(1111001, 20170903, null);
		
		mvc.perform(post(LANCAMENTOS_CONTABEIS_PATH)
					.content(new ObjectMapper().writeValueAsString(dto))
					.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testFindByIdSucesso() throws Exception {
		LancamentoContabilDTO dto = new LancamentoContabilDTO(1111001, 20170903, 100.00);
		when(service.findById("1")).thenReturn(dto);
		mvc.perform(get("/lancamentos-contabeis/1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("contaContabil", equalTo(1111001)))
			.andExpect(jsonPath("data", equalTo(20170903)))
			.andExpect(jsonPath("valor", equalTo(100.00)));
	}
	
	@Test
	public void testFindByIdComIdInexistente() throws Exception {
		when(service.findById("2"))
			.thenThrow(new ResourceNotFoundException("Nao ha lancamentos com esse Id."));
		mvc.perform(get("/lancamentos-contabeis/2"))
			.andExpect(status().isNotFound());
	}
	
	@Test
	public void testFindAllByContaSucesso() throws Exception {
		LancamentoContabilDTO dto = new LancamentoContabilDTO(1111001, 20170903, 100.00);
		List<LancamentoContabilDTO> lista = new ArrayList<>();
		lista.add(dto);
		when(service.findAllByConta(1)).thenReturn(lista);
		mvc.perform(get(LANCAMENTOS_CONTABEIS_PATH).param("contaContabil", "1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.[0].contaContabil", equalTo(1111001)))
			.andExpect(jsonPath("$.[0].data", equalTo(20170903)))
			.andExpect(jsonPath("$.[0].valor", equalTo(100.00)));
	}
	
	@Test
	public void testFindAllByContaSemParametro() throws Exception {
		mvc.perform(get(LANCAMENTOS_CONTABEIS_PATH))
			.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testFindAllByContaComContaInexistente() throws Exception {
		when(service.findAllByConta(3))
			.thenThrow(new ResourceNotFoundException("Nao ha lancamentos para essa conta."));
		mvc.perform(get("/lancamentos-contabeis/").param("contaContabil", "3"))
			.andExpect(status().isNotFound());
	}
	
	@Test
	public void testFindStatsByContaComSucesso() throws Exception {
		LancamentoContabilStatsDTO dto = new LancamentoContabilStatsDTO(190.00, 90.00, 100.00, 95.00, 2l);
		when(service.findStatsByConta(1)).thenReturn(dto);
		mvc.perform(get(LANCAMENTOS_CONTABEIS_STATS_PATH).param("contaContabil", "1"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("soma", equalTo(190.00)))
			.andExpect(jsonPath("maximo", equalTo(100.00)))
			.andExpect(jsonPath("minimo", equalTo(90.00)))
			.andExpect(jsonPath("media", equalTo(95.00)))
			.andExpect(jsonPath("quantidade", equalTo(2)));
	}
	
	@Test
	public void testFindStatsByContaComSucessoSemConta() throws Exception {
		LancamentoContabilStatsDTO dto = new LancamentoContabilStatsDTO(190.00, 90.00, 100.00, 95.00, 2l);
		when(service.findStatsByConta(null)).thenReturn(dto);
		mvc.perform(get(LANCAMENTOS_CONTABEIS_STATS_PATH))
			.andExpect(status().isOk())
			.andExpect(jsonPath("soma", equalTo(190.00)))
			.andExpect(jsonPath("maximo", equalTo(100.00)))
			.andExpect(jsonPath("minimo", equalTo(90.00)))
			.andExpect(jsonPath("media", equalTo(95.00)))
			.andExpect(jsonPath("quantidade", equalTo(2)));
	}
	
	@Test
	public void testFindStatsByContaSemLancamentos() throws Exception {
		when(service.findStatsByConta(null))
			.thenThrow(new ResourceNotFoundException("Nao ha lancamentos para gerar estatistica."));
		mvc.perform(get(LANCAMENTOS_CONTABEIS_STATS_PATH))
			.andExpect(status().isNotFound());
	}

}
